class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :course_name
      t.string :course_number
      t.string :section_number
      t.string :term
      t.string :instructor

      t.timestamps
    end
  end
end
