class CourseSerializer < ActiveModel::Serializer
  # has_many :assignments
  # belongs_to :user
  attributes :id, :course_name, :course_number, :section_number, :term, :instructor
end
